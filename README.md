# mkbackup

Copy the 'mkbackup' file to /usr/local/bin and set it as executable (chmod +x mkbackup).

The 'mkBackup.desktop' file is to be used with KDE.

For KDE4, copy this file to the $HOME/.kde/share/kde4/services/ directory.

For KDE5, copy this file to the $HOME/.local/share/kio/servicemenus/ directory.

The mkbackup script will then become available in Dolphin. When right-clicking on a file, you will be able to use the command "Actions / Copy file with a timestamp".
